# Block Riot

### RULES

![][cover]

## Game Objective
Take control of the city, even if it means ripping it apart at the same time!


By default all players can unite and win by taking over the town hall to defeat police. (This is the single player format)

Discussion is encouraged to win by town hall. Beware! Players have individual missions and can win for themselves...

Note: There is multiple ways to win, rioters may be in it for themselves...

**Take the Town hall**

All rioters race to take over the town hall

 - Consume +1 Town hall tile (See: 'Looting')
 - Players enter by moving in [costs 1 move]
 - Add 8 rioters & 1 leader

**Domination**

 - Prevent a rival's ability to move. Inherit an opponents loot.


### The Cause:
Randomly choose a cause at the start of the game to give yourself another way to win.

1. **Seize Leaders** in the police station on your block
 - 2 Leaders _in_ 2 player game,
 - 3 Leaders _in_ 3+ player game,
 - Trading 100 for each action card

2. **Ruinous**
 - Player must hold a building tile from each players block on the board. (4 tiles when 2 players)
 - Trading 80 for each action card

3. **Insurrection**
 - Player must capture a rival police station, town hall tile & 1 riot police
 - Trading 150 for 1 action card per turn

4. **City Regulator**
 - Player must occupy 3 city buildings from different player blocks
 - Trading 150 for 1 action card per turn

5. **Loot & Pillage**
 - Stockpile and hoard 900 of wealth
 - Trading 100 for each action card


------
# Setup - Getting started
Shuffle all tiles, buildings and cards then place face down.

## Board Layout

* **Town Hall** : Center map inside **Town Square** gardens
 - +1 riot police to each Town Hall corner

* **City Area** : Bordering the Town square

	#### City Building Tiles

	- Shuffle & Place City buildings face down.

## Players (Rioters & leaders)
 - **Player blocks (4)** : Purple (Anarchists), Red (Bikies), Green (Bogans) & yellow (Activists)

### Each player block starts with Building Tiles.
Shuffle 10 buildings, place randomly within player block

 - +2 police start on each police station
 - +1 car next to garage (Park directly outside, 2/3rds must be touching the garage)

### Each player starts with rioters:
 - _28 rioting civilians per player_
 - Each player starts with 4 Player action cards, keeps 2 _hidden_ then discards others
 - Start with 7 rioters and 1 _revealed_ leader in the corner of each players' block
 - A lead rioter(4 per player) is a rioting civilian who incites action. Leaders can remain hidden (to opponents) until found, captured or forced to reveal themselves by police action ```costs 1 move to hide or reveal```
![][rioters-front-back]
 - Top side shows player colour, when flipped leaders can be seen with a flag.
 - **Tip:** Place a spare flipped rioter to represent an inactive leader.

### Additional Units

EXPLAIN! trading building tile for a rioters
 - gaining territory, raising awareness, talking around the pub scene, pool halls

Call up more rioters to join your cause!

 - **Ends turn immediately**
 - Consumes 1 building tile, place on the stack
 - Costs 1 move to fetch each additional rioter, must be accompanied with a leader until all used, after that point player can fetch without the need of a leader.
 - Additional units can be placed at the start position in the corner of your block, in and around an **occupied** city buildings or split between the two.

------
# Game play

Player Energy/Courage is awarded every round, more can be obtained on a dice roll or from action cards. Courage is used for attacking, moving & looting. Police can intimidate players and the fear will remove some courage..

## Moving rioters
- Movement is limited by 3 or a dice roll. You may also gain additional moves from action cards.
- Rioters can move to any unoccupied 'space' on the grid (Stunned rioters **do not** occupy space, therefore can be trampled on by rivals or police)
- Courage is spent for each space traveled. EG: If you rolled a 6. You can choose to move 6 rioters once or 1 rioter 6 times.
- Courage is spent for flipping a rioter back to health, seize a rival, attacking police and entering a City Building

### Moving a crowd (Leaping)

An rioter cannot move diagonally when _'leaping'_ through a unit. They must be adjacent (side by side) with another rioter in the unit when it lands.

In order to leap, 2 or more rioters must be _'connected'_. From one end of a joined unit a rioter can leap all the way to the other end. Multiple rioters act as a **unit** when using the leap action. Leaping allows for radical shifts of movement in large groups.

```Picture illustration & youtube link coming ^^^ ```

## Player Turn
![][quick-start-1]
_Page 2 from the quick start guide_

## Political Campaign Fees (stack)

Mick Sucher the city governor seeks to tax the citizens to fund his political campaign and his personal finances. Players either pay or cop a nasty visit from the Police.

If the player chose to roll the dice during their turn, a police action card is charged against the player and loaded onto the stack. 

Before the turn ends the player can choose to defer the action by paying the fine amount shown (50 - 150). Player should pay with action cards, buildings or town hall tiles (to the same value or more).

Alternatively you may choose to collect the entire stack incurring all the damages found in the order shown.

Note: The stack naturally grows the more people roll the dice and pay charges, this continues until someone can no longer afford it or simply choose to collect.

**Collect the charges** Player also plunders the goods found in the stack!:

 - The police action is played instantly which **Immediately ends turn**
 - Player keeps any action cards and tiles **(2 tiles exchanged for 1 new action card from the deck)**
 - Collecting charges cannot be blocked

Players may choose to _defer_ the action to the "stack" by paying the amount shown

## Curfew

Flip the spinner to indicate the curfew has started.

_Said_ player who started the riot, rolls the dice (until another player starts a riot). Play returns to normal once _Said_ player rolls a 7 (ends turn). Next player gets the dice.

**During a curfew:**

 - All players move once per turn
 - Roll each turn

Note:
 - All police switch to riot police!
 - Police action cards do not apply
 - 2 Player action cards can be played during turn

**Curfew is triggered by:**

 - Attacking a riot police officer
 - Looting a town hall tile
 - Random Police Action


# Player Actions

## City Buildings

City buildings are a basic source of protection and serve as a command post to launch attacks from. They often hold gas masks and action cards, beware police may be lurking inside.

Rioters enter & exit city buildings perimeter at any point. City buildings are **'occupied' while 1 _revealed_ leader & 2 (or more) rioters are present**.

 - Once controlled the city building serves as an optional starting position for new units, spawning inside and from the perimeter outside of the city building.
 - **Note: Rivals can remove rioters already within building (see looting)**

## Attack and capture, rivals & police [Consumes 1-2 move(s)]

![][rioter-take]

 - 3 rioters can defeat rivals & police: Approach and surround your target [Any 2 sides while connected (3 sides if separate)]
 - By attacking rival rioters, they become _'Stunned'_ which takes 1 move, they can then be captured on the next move
 - When a player captures a rival rioter, they are held in **any player building**
 - Once **Police** are captured they are removed from play. _+1 Action Card, +1 Police action (played instantly)_
 - Once **Riot Police** are captured they are **flipped** and sent to the police station. _+1 Action Card, +1 Riot Police action (played instantly)_, **curfew begins.**

### Stunned rioters [Consumes 1 move]

When a rioter is inactive from police or gas, that rioter can be revived _(next turn)_. 
To do this the player moves an active rioter to nearest unoccupied space to 'touch' and flip back to active.

## Looting [Consumes 3 moves]
Players loot buildings, police stations and the Town Hall with a gang of 5.

 - To loot buildings surround your target with 5 or more rioters
 - When looting a town hall tile, riot police take action and a curfew begins. Ends Turn.

## Looting Cars
Each player starts with a car at their garage

* Joy ride!
	* Move 3 rioters on top the length of any vehicle
	* Car moves once for free
	* _Drive_ rioters and car to any open space (Except player start positions)
	* Must flip car (see below) Cannot move again
	* **Ends turn**

* Flipping a car - With 3 rioters on any vehicle
	* Flip car, **rioters remain on top**
	* When rioters leave, the space is obstructed and can no longer be used
	* +1 Action card
	* **Ends turn**

## Extraction [Consumes 1 move]
When a police or riot police captures a Leader, the Leader gets sent to the police station on the player's block.

To extract :

 - Remove police from police stations before rioters can be extracted. **Defeated Police are removed from the board** _+1 Action Card, +1 Police action_
 - Break out each captured rioter from police station.
 - Extract rivals and police from any building [town hall(anywhere along border), occupied city buildings, player buildings and police stations]


# Action Cards
The game lives and breathes with action cards, be aggressive, play smart by chaining many actions together.

Trade action cards in your hand with a new one from the deck.
 - The trade value is on the cause card you receive during setup.

## Player Action Cards
Players receive action cards when:

 - Building tiles looted
 - Riot Police are captured
 - Car is flipped
 - Can be traded according to the value of the player's cause card.

## Police Cards
Players control police as directed by police action cards. Use spinner where doubts arise.
A police card will be played during a turn when:

 - Any police are attacked
 - Indicated on building tile
 - Charges from rolling the dice

**Note: If police action has no effect, +1 Police token to _your_ police station**

## Riot Police Cards
 - **Played instantly**
 - **Ends player turn**
 - **Triggers the curfew**

# Police
Rioters have been known to go missing and die while in custody of police, especially when there are four or more. Protect your mob, by keeping police numbers down within their police stations, you're less likely to get caught off guard later.

Police remain inside the police stations found in player blocks until police action requires them to roam.

 - When police are placed over rioters they remain pinned underneath while police remain. Use the spinner if it is not clear where tokens should be placed.
 - Once police are attacked, remove from board.

## Police Stations:

	- When 4 police appear in the police station, current & future rioters perish while in custody (Remove from game play)
	- If police building reaches 6 police or more, 3 police exit the building, attack the nearest 3 rioters and stun
	- If a police station is removed from the board:
		- captured rioters are no longer protected and perish
		- or when any police added, immediately give chase & flip nearest rioter

## Riot Police:
Exist as the last line of defense, they have the power to restore order. Riot police remain inside the Town Square to defend the Town Hall. However have been known to roam troublesome player blocks.


------
**Stop! Boss Level:** If this is your first time playing Block Riot, you should get started now and refer back to the rules later in the game...

# Tear Gas (and Molotov):
Riot Police may deploy tear gas into units and territories. Once pin is released, all rioters in a 2 move radius are stunned. Rioters remain in this position until the gas clears. Retracting 1 square each turn until the base is gone. City buildings offer some protection, Rioters in buildings are unaffected unless police target city buildings specifically. 

![][tear-gas-area]

 - Riot Police and rioters with a mask are unaffected. (no flip)
 - Gas stuns rioters and flip police within the impact zone

 **Same applies for Molotov**

### Tear Gas Utilities

* **Gas Mask**

	![][gas-mask]
 - Can be moved within unit during turn
 - Protection against Tear Gas and Molotov

* **Tear Gas Canister (and Molotov)**

	![][utilities]

	- Has 3 tokens and lasts 3 turns
	- Canisters remain in position for 3 turns. Each turn a canister piece is removed.


Good luck and have fun!

------
#Story
------

_Because oppression isn't going to kick its own ass!_

Block Riot is an adventure strategy game of skill and chance. You need to balance caution and aggression when it's time to rumble.

Police resistance is rife within the block, players constantly face-off against police and rivals.

Fight your way to the town hall and take control of the incompetent & corrupt government. Meanwhile your opponents are hunting in the shadows with their own selfish causes. You need to stay ahead of rivals at every turn.

Race to gain control of a city that has lost its way. The fleeing government has spent more than budgeted and where the money has gone is in question, rioters strike and grapple to regain control among the 4 distinct groups, all pushing their own cause, only corrupt cops remain to hand out heavy handed punishments and collect cash for crimes never committed



Co-op objective:
The warm-up game will be a co-op ‘TOWN HALL’ takeover to win!

"Now the freedom of speech is being strongly eroded, and this is the critical time we hope the activists, bikies, bogans and anarchists will stay with us..."
 - read by a news reporter 

2 players facing..

""what if we make em think were going to take the town hall, but instead we take their leaders captive?" 

"...good idea why didn't i think of that"

chuckles..



Discuss, borders between Blocks, town square parks, city buildings and town hall.

There are 4 Leaders within each block, they play a key roll to winning games and have the ability to command city buildings & town halls. But be careful as they also are targeted by police and rivals



#Factions and Causes

###Green
The peaceful people of leafy Boganville have enjoyed many generations of prosperous agriculture, however now foreign investors are buying up fertile farm land.  

To make things worse they are being systematically uprooted by council to make way for a new highway that leads to a proposed power plant.  Under an act signed 1812, The government claim that the land does not belong to the residents...

###Yellow
At the bay, descendants of natives and early settlers have united, putting their differences aside. These people live in a beautiful land, however due to recent natural disasters they are left without clean drinking water. 

The government has a history of prejudice against this community, which has seen many fall behind by a lack of support and education. They can no longer afford the hyper inflated life like the other blocks do. 

They fear their culture will be lost to the newer wealthy blocks

Signboard:
“Why do we have to keep telling you?”

###Purple
Anarchists are expanding and require more power to service their needs, and are lobbying government to build a new power plant nearby

Purple are the offspring of wealthy new migrants and are conservative by nature, they bring wealth to Knoxville from a foreign land. They plan to keep their city the way it is and are known to be aggressive with anyone who stands in the way of their progress including local authorities! 

Purple claim the police are corrupt and not upholding the law, they vie to take over and lead their way...

###Red
Meanwhile, in the red block...
Police are increasingly harassing the citizens here and many have joined the local bikie gang. They claim  police are restrictive and punishment is served harshly and frequently. Many believe this to be revenue raising for an over conservative government. 

The ever changing laws are controlling and manipulating citizens. Citizens are proud, want freedom and are willing to RIOT for it... 
-------------------------------------------------
Sign board reads:“who are you going to call when the police murder?”


###Police

Policeman says
“Peaceful protests are allowed but only to 3 moves per turn”

“If you are found to be rolling the dice then you will be fined and if you do not pay, action will be taken against you”

Rioter says:
“if we just surround them one by one we can get rid of them once and for all!”


###Politics and government
The charges stack
"If the city does fall to the police, the free world will lose the safeguard that has been keeping corruption at everybody's doorstep"


[cover]: http://interludic.com.au/blockriot_assets/images/cover.png
[board-buildings-th]: http://interludic.com.au/blockriot_assets/images/board-buildings-th.jpg
[city-tile-hidden]: http://interludic.com.au/blockriot_assets/images/city-tile-hidden.JPG
[city-tile-revealed]: http://interludic.com.au/blockriot_assets/images/city-tile-revealed.JPG
[rioters-front-back]: http://interludic.com.au/blockriot_assets/images/rioters-front-back.jpg
[gas-mask]: http://interludic.com.au/blockriot_assets/images/gas-mask.jpg
[tear-gas-area]: http://interludic.com.au/blockriot_assets/images/tear-gas-area.jpg
[rioter-take]: http://interludic.com.au/blockriot_assets/images/element-take.jpg
[units-leaping]: http://interludic.com.au/blockriot_assets/images/units-leaping.jpg
[utilities]: http://interludic.com.au/blockriot_assets/images/utilities.jpg
[quick-start-1]: http://interludic.com.au/blockriot_assets/images/quick-start-1@2x.png


------
BlockRiot - rules v0.7.3 / [Interludic](http://info@interludic.com.au) "We Will Play" - Copyright (c) 2020

------