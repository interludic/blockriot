Moved to illustrator

# Quick Guide

## Player Turn
### One of the following activities
Roll the dice and move the amount shown. Police 'charges' apply.
Pay fine or collect stack at the end of turn

OR

Play your Action cards


## Players Can Attack
 - Police (3 elements / 1 move) __+1 Action Card +1 Police action__
 - Riot Police (3 elements / 1 move) __+1 Action Card +1 Police Riot action__
 - Rival elements & Leaders (3 elements / 1 move)

 - Player Buildings or town hall (5 elements / 3 moves)
 - Occupy City Buildings (2 elements +1 Leader revealed)
 - Flip Cars (3 elements / 3 moves) __+1 Action card__

## City Buildings
Enter City Buildings (1 move)
To occupy, elements enter through city building perimeter, once inside obey exits.
* 1 revealed leader & 2 elements

### Offers:
 - Protection from gas
 - New units can now start from here instead

## Rioting
 See compass


-- Block Riot - Quick Guide v.0.7.1 --